require("dotenv").config();

export default {
    jwtSecret: process.env.JWT_SECRET || 'some_secret_tocken',
    DB: {
        URI: `mongodb+srv://${process.env.USER}:${process.env.PASSWORD}@cluster0.0kfw6.mongodb.net/${process.env.DATABASE}?retryWrites=true&w=majority`,
        USER: process.env.USER,
        PASSWORD: process.env.PASSWORD,
        DATABASE: process.env.DATABASE
    }
}