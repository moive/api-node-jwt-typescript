import mongoose, {ConnectionOptions} from "mongoose";

import config from "./config/config";
const chalk = require("chalk");

const dbOptions: ConnectionOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
};

mongoose.connect(config.DB.URI, dbOptions);

const connection = mongoose.connection;

connection.once("open", () => {
    console.log(
        chalk.underline.rgb(255, 0, 255)("Mongodb connection stablished")
    );
});

connection.on("error", (err) => {
    console.log(err);
    process.exit(0);
});
